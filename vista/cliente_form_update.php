<!-- Bread crumb -->
<div class="row page-titles">
    <div class="col-md-5 align-self-center">
        <h3 class="text-primary">Clientes</h3> </div>
    <div class="col-md-7 align-self-center">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
            <li class="breadcrumb-item active">Clientes</li>
            <li class="breadcrumb-item active">Editar Cliente</li>
        </ol>
    </div>
</div>
<!-- End Bread crumb -->
<!-- Container fluid  -->
<div class="container-fluid">
    <!-- Start Page Content -->
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-6">
                            <h3 class="box-title">
                                Editar Cliente
                            </h3>
                        </div>
                        <div class="col-md-6" align="right">
                            <button class="cancel btn-danger" onclick="loadContent('../vista/usuarios_index.php')" ><i class="fa fa-times"></i></button>
                        </div>
                    </div>
                    <form onsubmit="return false;" id="form_cliente_up">
                        <div class="row">
                            <div class="col-md-6 col-xs-12">
                                <label for="">Nombre Cliente:</label> <br>
                                <label></label>
                                <input class="form-control" type="hidden" name="id_cliente" id="id_cliente"  />
                                <input class="form-control" type="text" name="ncliente" id="ncliente" required />
                            </div>
                            <div class="col-md-6 col-xs-12">
                                <label for="">Codigo Cliente / Rif / DNI:</label> <br>
                                <label></label>
                                <input class="form-control" type="text" name="codigo" id="codigo" required />
                            </div>
                        </div>
                        <br />
                        <div class="row">
                            <div class="col-md-6 col-xs-12">
                                <label for="">Telefono:</label> <br>
                                <label></label>
                                <input class="form-control" type="text" name="telefono" id="telefono" required />
                            </div>
                            <div class="col-md-6 col-xs-12">
                                <label for="">Url acceso:</label> <br>
                                <label></label>
                                <input class="form-control" type="text" name="acceso" id="acceso" required />
                            </div>
                        </div>
                        <br />
                        <div class="row">
                            <div class="col-md-12 col-xs-12">
                                <label for="">Dirección:</label> <br>
                                <label></label>
                                <textarea class="form-control" type="text" name="direccion" id="direccion" required ></textarea>
                            </div>
                        </div>
                        <br />
                        <div class="row">
                            <div class="col-md-12 col-xs-12" align="right">
                                <button class="btn btn-lg btn-success"><i class="fa fa-user-plus"></i> Guardar </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- End PAge Content -->
</div>
<link rel="stylesheet" href="../assets/css/form.style.css" />
<script type="text/javascript" src="../assets/js/module_cliente.js"></script>
