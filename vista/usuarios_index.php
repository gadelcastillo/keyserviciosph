<?php
    require_once '../modelo/Conexion.php';
    $pdo = new Conexion();
?>
<!-- Bread crumb -->
<div class="row page-titles">
    <div class="col-md-5 align-self-center">
        <h3 class="text-primary">Usuarios</h3> </div>
    <div class="col-md-7 align-self-center">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
            <li class="breadcrumb-item active">Usuarios</li>
        </ol>
    </div>
</div>
<!-- End Bread crumb -->
<!-- Container fluid  -->
<div class="container-fluid">
    <!-- Start Page Content -->
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                  <div id="table_usuarios" class="row">
                    <div class="card-title">
                      <div class="col-md-6 col-xs-12" align="left">
                          <button class="btn btn-primary" onclick="loadContent('../vista/usuarios_form.php')"><i class="fa fa-plus-circle fa-fw"></i> Nuevo Usuario</button>
                      </div>
                      </div>
                      <div class="col-md-6 col-xs-12" align="right">
                          <form>
                              <div class="input-group input-group-rounded" align="right">
                                  <input class="isearch form-control search" type="text" placeholder=" Buscar Usuario">
                                  <span class="input-group-btn"><span class="btn btn-primary btn-group-right"><i class="ti-search"></i></span></span>
                              </div>
                          </form>
                      </div>

                      <div class="col-md-12">
                        <div class="table-responsive">
                          <table class="table">
                              <thead>
                                  <th align="center">Username</th>
                                  <th align="center">Nombres</th>
                                  <th align="center">DNI</th>
                                  <th align="center">Fecha de ingreso</th>
                                  <th align="center">Acción</th>
                              </thead>
                              <tbody class="list">
                              <?php

                                  $query = "SELECT * FROM usuarios";
                                  $sql = $pdo->query($query);

                                  while( $result = $sql->fetch(PDO::FETCH_ASSOC) ){
                                      echo "<tr>
                                              <td class='user' >".$result['username']."</td>
                                              <td class='nombre' >".$result['nomape']."</td>
                                              <td class='dni' >".$result['dni']."</td>
                                              <td>".$result['fec_ingreso']."</td>
                                              <td>
                                                  <div class='dropdown'>
                                                      <button class='btn btn-primary dropdown-toggle' type='button' id='dropdownMenu1' data-toggle='dropdown' aria-haspopup='true' aria-expanded='true'>
                                                          Opciones
                                                          <span class='fa fa-cog' aria-hidden='true'></span>
                                                      </button>
                                                      <ul class='dropdown-menu' aria-labelledby='dropdownMenu1'>
                                                          <li onclick='editUser(".$result['id_user'].")'>
                                                              <a href='#'>
                                                                  Editar <i class='fa fa-pencil-square-o' aria-hidden='true'></i>
                                                              </a>
                                                          </li>
                                                          <li onclick='deleteUser(".$result['id_user'].")'>
                                                              <a href='#'>
                                                                  Eliminar <i class='fa fa-trash-o' aria-hidden='true'></i>
                                                              </a>
                                                          </li>
                                                      </ul>
                                                  </div>
                                              </td>
                                            </tr>";
                                  }
                              ?>
                              </tbody>
                          </table>
                        </div>
                          <ul class="pagination"></ul>
                      </div>
                  </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End PAge Content -->
</div>
<script type="text/javascript" src="../assets/js/module_usuarios.js"></script>
<script type="text/javascript" src="../assets/js/list.min.js"></script>
<script type="text/javascript">
    var optionws = {
        valueNames:["user","nombre","dni"],
        pagination: true,
        page:10
    };

    var Userlist = new List("table_usuarios", optionws);
</script>
