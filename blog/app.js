var app = angular.module('app-blog', ['ui.bootstrap', 'oitozero.ngSweetAlert']);

app.controller('PageCtrlr', function($scope , $http) {
  $scope.filteredPosts = []
  ,$scope.currentPage = 1
  ,$scope.numPerPage = 5
  ,$scope.maxSize = 5
  var begin = (($scope.currentPage - 1) * $scope.numPerPage);
  var  end = begin + $scope.numPerPage;

    
  $scope.getPosts = function() {
    $scope.Posts = [];
    $http({
      method: 'GET',
      url: '../controlador/dataPost.php'
    }).then( function Success (response){
        $scope.Posts = response.data ;
    }, function Error (response){
      console.log(response.statusText);
    });
  };
  $scope.getPosts(); 

  $scope.filteredPosts = $scope.Posts.slice(begin, end);

  $scope.numPages = function () {
    return Math.ceil($scope.Posts.length / $scope.numPerPage);
  };
  
  $scope.$watch('currentPage + numPerPage + Posts', function(Posts) {
    begin = (($scope.currentPage - 1) * $scope.numPerPage);
    end = begin + $scope.numPerPage;
    
    $scope.filteredPosts = $scope.Posts.slice(begin, end);
  
  });
  
});

app.controller('SubcriptionsCtrl' , function($scope , $http){
    
    $scope.formData = {} ;

    $scope.saveSubscriber = function () {
        
        $http({
            method : 'POST' ,
            url : '../controlador/saveSubscriber.php' ,
            data : $.param($scope.formData) ,
            headers : { 'Content-type' : 'application/x-www-form-urlencoded' }
        }).then( function Success (response){

            if(response.data == 1){
                alert("Gracias por subcribirse con nosotros!");
                console.log(response);
                $scope.formData = {};
            }
            if(response.data == 2){
                alert("El correo ya se encuentra registrado!");
                console.log(response.data);
                $scope.formData = {} ;
            }

        }, function Error (error){
            alert("Ha ocurrido un error con su subscripción, por favor intentelo de nuevo!");
            console.log(error.data);
        });
    }
});

