var progressBar = $('div.progress-bar');

function wizardSuccess($div , $porcentaje){

  var formulario = $('#'+$div) ;

  formulario.removeClass('active');

  formulario.next().addClass('active');

  progressBar.css({width:$porcentaje+'%'}).html($porcentaje+'%');

}

function wizardFailure($div){

  var formulario = $('#'+$div) ;

  formulario.removeClass('active');

  formulario.next().addClass('active');

  progressBar.css({width:'100%'}).html('Error');

  progressBar.removeClass('bg-success');

  progressBar.addClass('bg-danger');

}
