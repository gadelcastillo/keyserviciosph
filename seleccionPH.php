<?php
    require 'modelo/Conexion.php';
    $pdo = new Conexion();
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title> Key's Servicios y Soluciones </title>
        <meta name="description" content="Key's servicios y soluciones, administracion de propiedades horizontales" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
        <meta name="generator" content="Codeply">
        <link rel="shortcut icon" type="image/x-icon" href="assets/img/favicon.png" >
        <link rel="stylesheet" href="assets/css/bootstrap.min.css" />
        <link rel="stylesheet" href="assets/css/animate.min.css" />
        <link rel="stylesheet" href="assets/css/ionicons.min.css" />
        <link rel="stylesheet" href="assets/css/font-awesome.min.css" />
        <!--<link rel="stylesheet" href="assets/css/styles.css" />-->
        <link rel="stylesheet" href="assets/css/loginstyle.css" />
        <link rel="stylesheet" href="assets/css/sweetalert.css" />
        <link rel="stylesheet" href="assets/css/google.css" />
    <head>
    <body>
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-10 col-sm-offset-1 col-md-6 col-md-offset-3">
                    <div class="form-group central">
                        <form onsubmit="return false;" id="login">
                            <div class="row">
                                <h2>Favor seleccionar su condominio correspondiente</h2>
                                <div class="col-md-12">
                                    <select id="seleccionPh" class="form-control">
                                        <option value="*" selected>Seleccione su PH</option>
                                        <?php
                                            $sql = $pdo->query("SELECT id_cliente , nombre_cliente FROM clientes");
                                            while($results = $sql->fetch(PDO::FETCH_ASSOC)){
                                                echo '<option value="'.$results["id_cliente"].'">'.$results["nombre_cliente"].'</option>';
                                            }
                                        ?>
                                    </select>
                                </div>
                                <div class="col-md-6 col-xs-6" align="center">

                                        <button class="btn btn-danger">
                                        <a  href="index.php" style="text-decoration:none; color:white;">
                                            <i class="fa fa-step-backward" aria-hidden="true"></i> Regresar </a>
                                        </button>

                                </div>
                                <div class="col-md-6 col-xs-6" align="center">
                                    <button onclick="seleccion()" class="btn btn-success"><i class="fa fa-building-o" aria-hidden="true"></i>     Ir</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

        <div>


        <!--scripts loaded here -->
        <script src="assets/js/jquery.min.js"></script>
        <script src="assets/js/validate.min.js"></script>
        <script src="assets/js/bootstrap.min.js"></script>
        <script src="assets/js/jquery.easing.min.js"></script>
        <script src="assets/js/wow.js"></script>
        <script src="assets/js/scripts.js"></script>
        <script src="assets/js/functions.js" ></script>
        <script src="assets/js/sweetalert.min.js" ></script>

    </body>
</html>
