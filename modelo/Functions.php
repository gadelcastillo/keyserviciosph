<?php
    
    /**
     * Funcion para agregar la direccion url de la imagen recien agregada 
     * en la bd temporal 
     */
    function updateTemp($id , $value){
        $con = new Conexion();
        try{
            $query = $con->prepare("UPDATE temp_publicidad SET  direc_t = :val WHERE  id_t = :id");
            $query->bindParam(':val' , $value);
            $query->bindParam('id' , $id);

            $query->execute();

            return true;

        }catch(PDOExeption $e){

            return false;

        } 
    }

    function setAdvertisement($id){
        $con = new Conexion();
        try{

            $getData = $con->prepare("SELECT * FROM temp_publicidad WHERE id_t = :id");
            $getData->bindParam(':id' , $id);
            $getData->execute();
            $temp = $getData->fetch(PDO::FETCH_ASSOC);

            if(count($temp) > 0){

                $setData = $con->prepare("INSERT INTO publicidad (direccion_img, nombre_pu , date_i , date_f) 
                                            VALUES(:direc_t , :name_t , :dates_t , :datef_t)");
                $setData->bindParam(':direc_t' , $temp['direc_t']);
                $setData->bindParam(':name_t' , $temp['name_t']);
                $setData->bindParam(':dates_t' , $temp['dates_t']);
                $setData->bindParam(':datef_t' , $temp['datef_t']);
                $setData->execute();

                return true;

            }else{

                return false;

            }
        }catch(PDOExeption $e){

            return false; 

        }
    }

    function deleteTemp($id){
        $con = new Conexion();
        try{
            $deleteData = $con->prepare("DELETE FROM temp_publicidad WHERE id_t = :id");
            $deleteData->bindParam(':id' , $id);
            $deleteData->execute();

            return true;

        }catch(PDOExeption $e){

            return false;

        }
    }
?>