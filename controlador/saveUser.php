<?php

    require '../modelo/Conexion.php';
    $pdo = new Conexion();
    
    try{

        $query = $pdo->prepare("INSERT INTO usuarios (username , password , nomape , dni)
        VALUES (:username, :pass, :nomape, :dni)");
        $pass = sha1($_POST['pass1']);
        $query->bindParam(':username',$_POST['username']);
        $query->bindParam(':pass', $pass );
        $query->bindParam(':nomape',$_POST['nomape']);
        $query->bindParam(':dni',$_POST['dni']);
    
        $query->execute();

        $res = new stdClass();
        $res->icon = "success";
        $res->titulo = "Operacion Exitosa";
        $res->msj = "Usuario agregado de forma exitosa";
        $res->btn = "#66BB6A";
        $res->content = "../vista/usuarios_index.php";			
        $json = json_encode($res);
        echo $json;

    }catch(PDOException $e){

        $res = new stdClass();
        $res->icon = "warning";
        $res->titulo = "Falla al grabar en BD";
        $res->msj = "Ha ocurrido un error, por favor comunicarse con el administrador del sistema!";
        $res->btn = "#EF5350";
        $res->content = "../vista/usuarios_index.php";			
        $json = json_encode($res);
        echo $json;

    }
    

?>