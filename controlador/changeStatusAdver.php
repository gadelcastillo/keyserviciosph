<?php
    require '../modelo/Conexion.php';

    $pdo = new Conexion();

    $publicidad = $_POST['id_publicidad'];
    $status = $_POST['status'];

    if($status == 0){
        try{
            $query = $pdo->prepare("UPDATE publicidad SET status = 1 WHERE id_publicidad = :id");
            $query->bindParam('id' , $publicidad);
            $query->execute();

            $res = new stdClass();
            $res->icon = "success";
            $res->titulo = "Operacion Exitosa";
            $res->msj = "Estado cambiado correctamente!";
            $res->btn = "#66BB6A";	
            $res->content = "../vista/patrocinador_index.php";		
            $json = json_encode($res);

            echo $json;
        }
        catch(PDOexeption $e){

            $res = new stdClass();
            $res->icon = "warning";
            $res->titulo = "Falla en la BD";
            $res->msj = "Ha ocurrido un error, por favor comunicarse con el administrador del sistema!";
            $res->btn = "#EF5350";	
            $res->content = "../vista/patrocinador_index.php";		
            $json = json_encode($res);
            
            echo $json;
        }
    }
    if($status == 1){
        try{
            $query = $pdo->prepare("UPDATE publicidad SET status = 0 WHERE id_publicidad = :id");
            $query->bindParam('id' , $publicidad);
            $query->execute();

            $res = new stdClass();
            $res->icon = "success";
            $res->titulo = "Operacion Exitosa";
            $res->msj = "Estado cambiado correctamente!";
            $res->btn = "#66BB6A";	
            $res->content = "../vista/patrocinador_index.php";		
            $json = json_encode($res);

            echo $json;
        }
        catch(PDOexeption $e){

            $res = new stdClass();
            $res->icon = "warning";
            $res->titulo = "Falla en la BD";
            $res->msj = "Ha ocurrido un error, por favor comunicarse con el administrador del sistema!";
            $res->btn = "#EF5350";	
            $res->content = "../vista/patrocinador_index.php";		
            $json = json_encode($res);
            
            echo $json;
        }
    }
?>