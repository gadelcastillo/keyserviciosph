<?php

    require '../modelo/Conexion.php';
    $pdo = new Conexion();
    
    try{

        $query = $pdo->prepare("UPDATE clientes 
                                    SET nombre_cliente = :nombre , 
                                        nit = :nit , 
                                        direccion = :direccion ,
                                        telefono = :telefono, 
                                        url_sistema = :url_sistema
                                WHERE id_cliente = :cliente");

        $query->bindParam(':cliente' ,$_POST['id_cliente']);
        $query->bindParam(':nombre',$_POST['ncliente']);
        $query->bindParam(':nit',$_POST['codigo']);
        $query->bindParam(':direccion',$_POST['direccion']);
        $query->bindParam(':telefono',$_POST['telefono']);
        $query->bindParam(':url_sistema',$_POST['acceso']);
    
        $query->execute();

        $res = new stdClass();
        $res->icon = "success";
        $res->titulo = "Operacion Exitosa";
        $res->msj = "Cliente actualizado de forma exitosa";
        $res->btn = "#66BB6A";
        $res->content = "../vista/cliente_index.php";			
        $json = json_encode($res);
        echo $json;

    }catch(PDOException $e){

        $res = new stdClass();
        $res->icon = "warning";
        $res->titulo = "Falla al grabar en BD";
        $res->msj = "Ha ocurrido un error, por favor comunicarse con el administrador del sistema!";
        $res->btn = "#EF5350";
        $res->content = "../vista/cliente_index.php";			
        $json = json_encode($res);
        echo $json;

    }
    

?>