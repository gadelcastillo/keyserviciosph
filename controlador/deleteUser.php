<?php

    require '../modelo/Conexion.php';
    $pdo = new Conexion();

    $id_usuario = $_POST['id_usuario'];

    $query = $pdo->prepare("DELETE FROM usuarios WHERE id_user = :user ");

    try{

        $query->bindParam(':user' , $id_usuario);
        $sql = $query->execute();

        $res = new stdClass();
        $res->icon = "success";
        $res->titulo = "Operacion Exitosa";
        $res->msj = "Usuario eliminado de forma exitosa";
        $res->btn = "#66BB6A";	
        $res->content = "../vista/usuarios_index.php";		
        $json = json_encode($res);
        echo $json;


    }catch(PDOException $e){

        $res = new stdClass();
        $res->icon = "warning";
        $res->titulo = "Falla al borrar en BD";
        $res->msj = "Ha ocurrido un error, por favor comunicarse con el administrador del sistema!";
        $res->btn = "#EF5350";	
        $res->content = "../vista/usuarios_index.php";		
        $json = json_encode($res);
        echo $json;

    }



?>