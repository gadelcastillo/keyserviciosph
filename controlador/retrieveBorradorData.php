<?php
    session_name('temp_noticias');
    session_start();

    require '../modelo/Noticia.php' ;
    
    $borrador = new Noticia($_POST['idBorrador']) ;

    $data = $borrador->loadBorrador();

    $_SESSION['LastId'] = $data['id_temp'];

    $_SESSION['Titulo'] = $data['titulo'];

    $json = json_encode($data);

    echo $json ; 

?>