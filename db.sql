-- Base de datos: `keyserviciosassets`
-- 

-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `clientes`
-- 

DROP TABLE IF EXISTS `clientes`;
CREATE TABLE IF NOT EXISTS `clientes` (
  `id_cliente` int(11) NOT NULL AUTO_INCREMENT,
  `nombre_cliente` varchar(100) DEFAULT NULL,
  `nit` varchar(100) DEFAULT NULL,
  `direccion` varchar(150) DEFAULT NULL,
  `telefono` varchar(50) DEFAULT NULL,
  `url_sistema` varchar(100) DEFAULT NULL,
  `fec_ingreso` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_cliente`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

-- 
-- Volcar la base de datos para la tabla `clientes`
-- 

INSERT INTO `clientes` (`id_cliente`, `nombre_cliente`, `nit`, `direccion`, `telefono`, `url_sistema`, `fec_ingreso`) VALUES (6, 'PH Miraluna', '000001', 'Av. Ernesto T Lefevre. PH Miraluna. Ciudad de PanamÃ¡, PanamÃ¡.', '', 'condovive.com/keysserviciosysoluciones/phmiraluna/', '2017-11-27 16:34:17');
INSERT INTO `clientes` (`id_cliente`, `nombre_cliente`, `nit`, `direccion`, `telefono`, `url_sistema`, `fec_ingreso`) VALUES (7, 'PH Parque Lindo I', '00002', 'Av. Ernesto T Lefevre, PH Parque Lindo I. Ciudad de PanamÃ¡, PanamÃ¡.', '', 'condovive.com/keysserviciosysoluciones/phparquelindo/', '2017-11-27 21:05:21');
INSERT INTO `clientes` (`id_cliente`, `nombre_cliente`, `nit`, `direccion`, `telefono`, `url_sistema`, `fec_ingreso`) VALUES (8, 'PH Premier Plaza', '0003', 'Calle 8 Carasquilla, PH Premier Plaza. Ciudad de PanamÃ¡, PanamÃ¡.', '', 'condovive.com/keysserviciosysoluciones/phpremierplaza/', '2017-11-28 14:25:31');
INSERT INTO `clientes` (`id_cliente`, `nombre_cliente`, `nit`, `direccion`, `telefono`, `url_sistema`, `fec_ingreso`) VALUES (9, 'PH 19th Road', '0004', 'Ciudad de PanamÃ¡, PanamÃ¡.', '', 'condovive.com/keysserviciosysoluciones/ph19road/', '2017-11-28 14:30:39');
INSERT INTO `clientes` (`id_cliente`, `nombre_cliente`, `nit`, `direccion`, `telefono`, `url_sistema`, `fec_ingreso`) VALUES (10, 'PH Terrazas del Bosque', '0005', 'Ciudad de PanamÃ¡, PanamÃ¡.', '', 'condovive.com/keysserviciosysoluciones/phterrazasdelbosque/', '2017-11-28 14:32:44');
INSERT INTO `clientes` (`id_cliente`, `nombre_cliente`, `nit`, `direccion`, `telefono`, `url_sistema`, `fec_ingreso`) VALUES (11, 'PH Parque Lindo II', '0006', 'Ciudad de PanamÃ¡, PanamÃ¡.', '', 'condovive.com/keysserviciosysoluciones/parquelindoii/', '2017-12-04 18:15:19');

-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `publicidad`
-- 

DROP TABLE IF EXISTS `publicidad`;
CREATE TABLE IF NOT EXISTS `publicidad` (
  `id_publicidad` int(11) NOT NULL AUTO_INCREMENT,
  `direccion_img` varchar(100) DEFAULT NULL,
  `nombre_pu` varchar(50) DEFAULT NULL,
  `date_i` date DEFAULT NULL,
  `date_f` date DEFAULT NULL,
  `status` varchar(2) DEFAULT '1',
  PRIMARY KEY (`id_publicidad`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

-- 
-- Volcar la base de datos para la tabla `publicidad`
-- 


-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `temp_publicidad`
-- 

DROP TABLE IF EXISTS `temp_publicidad`;
CREATE TABLE IF NOT EXISTS `temp_publicidad` (
  `id_t` int(11) NOT NULL AUTO_INCREMENT,
  `name_t` varchar(45) DEFAULT NULL,
  `dates_t` date DEFAULT NULL,
  `datef_t` date DEFAULT NULL,
  `direc_t` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id_t`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1 COMMENT='Esta tabla es estrictamente para contener la informacion de forma temporal en la base de datos mientras la imagen es cargada al server ' AUTO_INCREMENT=2 ;

-- 
-- Volcar la base de datos para la tabla `temp_publicidad`
-- 


-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `usuarios`
-- 

DROP TABLE IF EXISTS `usuarios`;
CREATE TABLE IF NOT EXISTS `usuarios` (
  `id_user` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `nomape` varchar(100) NOT NULL,
  `dni` varchar(50) NOT NULL,
  `fec_ingreso` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_user`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

-- 
-- Volcar la base de datos para la tabla `usuarios`
-- 

INSERT INTO `usuarios` (`id_user`, `username`, `password`, `nomape`, `dni`, `fec_ingreso`) VALUES (2, 'KEYADMIN', 'e74e3496c21040b65aed3482ccbf8357d3c247f9', 'Gustavo Del Castillo', '21515424', '2018-01-03 20:43:46');
INSERT INTO `usuarios` (`id_user`, `username`, `password`, `nomape`, `dni`, `fec_ingreso`) VALUES (3, 'JHUGGINS', 'da39a3ee5e6b4b0d3255bfef95601890afd80709', 'Johanna Huggins', '142078644', '2018-01-03 20:55:09');
INSERT INTO `usuarios` (`id_user`, `username`, `password`, `nomape`, `dni`, `fec_ingreso`) VALUES (4, 'GHUGGINS', 'b723fa897951d1e3406e03a1528edd2636f6a406', 'Gricely Huggins', '125692261', '2018-01-03 20:49:04');
